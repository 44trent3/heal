package com.minecraftonline.heal;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class HealCommand implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource source, CommandContext args) throws CommandException {
        if (source instanceof Player) {
            Player player = (Player) source;

            if(player.getHealthData().health().get() < player.getHealthData().health().getMaxValue()) {
                player.offer(Keys.HEALTH, player.getHealthData().health().getMaxValue());
                source.sendMessage(Text.of(TextColors.DARK_GREEN, "You have been healed!"));
            } else {
                source.sendMessage(Text.of(TextColors.DARK_GREEN, "You are already at full health."));
            }
            return CommandResult.success();
        }
        if (!(source instanceof Player)) {
            source.sendMessage(Text.of(TextColors.RED, "The console cannot be healed."));
        }
        return CommandResult.empty();
    }
}

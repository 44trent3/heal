package com.minecraftonline.heal;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

public class HealCommand implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource source, CommandContext args) throws CommandException {
        if (source instanceof Player) {
            Optional<Player> player = args.getOne("player");
            if (player.isPresent()) {
                if (player.get().getHealthData().health().get() < player.get().getHealthData().health().getMaxValue()) {
                    player.get().offer(Keys.HEALTH, player.get().getHealthData().health().getMaxValue());
                    player.get().offer(Keys.FIRE_TICKS, 0);
                    player.get().sendMessage(Text.of(TextColors.DARK_GREEN, "You have been healed by " + source.getName(),"!"));
                    source.sendMessage(Text.of(TextColors.DARK_GREEN, "They have been healed."));
                } else if(player.get().getHealthData().health().get() >= player.get().getHealthData().health().getMaxValue()) {
                    source.sendMessage(Text.of(TextColors.DARK_GREEN, "They are already at full health!"));
                }
            } else {
                Player src = (Player)source;
                if(src.getHealthData().health().get() < src.getHealthData().health().getMaxValue()) {
                    src.offer(Keys.HEALTH, src.getHealthData().health().getMaxValue());
                    src.offer(Keys.FIRE_TICKS, 0);
                    src.sendMessage(Text.of(TextColors.DARK_GREEN, "You have been healed!"));
                } else if(src.getHealthData().health().get() >= src.getHealthData().health().getMaxValue()) {
                    src.sendMessage(Text.of(TextColors.DARK_GREEN, "You are already at full health!"));
                }
                return CommandResult.success();
            }
            return CommandResult.empty();
        }
        if (source instanceof ConsoleSource) {
            Optional<Player> player = args.getOne("player");
            if(player.isPresent()) {
                if(player.get().getHealthData().health().get() < player.get().getHealthData().health().getMaxValue()) {
                    player.get().offer(Keys.HEALTH, player.get().getHealthData().health().getMaxValue());
                    player.get().offer(Keys.FIRE_TICKS, 0);
                    player.get().sendMessage(Text.of(TextColors.DARK_GREEN, "You have been healed!"));
                } else if(player.get().getHealthData().health().get() >= player.get().getHealthData().health().getMaxValue()) {
                    source.sendMessage(Text.of(TextColors.DARK_GREEN, "They are already at full health!"));
                }
                return CommandResult.success();
            }
            source.sendMessage(Text.of(TextColors.DARK_RED, "You can't heal the console!"));
        }
        return CommandResult.empty();
    }
}

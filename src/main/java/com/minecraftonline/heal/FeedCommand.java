package com.minecraftonline.heal;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

public class FeedCommand implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource source, CommandContext args) throws CommandException {
        if (source instanceof Player) {
            Optional<Player> player = args.getOne("player");
            if(player.isPresent()) {
                if(player.get().getFoodData().foodLevel().get() < 20) {
                    player.get().offer(Keys.FOOD_LEVEL, 20);
                    player.get().sendMessage(Text.of(TextColors.DARK_GREEN, "You have been fed by " + source.getName(),"!"));
                    source.sendMessage(Text.of(TextColors.DARK_GREEN, "They have been fed."));
                } else if(player.get().getFoodData().foodLevel().get() >= 20) {
                    source.sendMessage(Text.of(TextColors.DARK_GREEN, "They are already full!"));
                }
            } else {
                Player src = (Player)source;
                if(src.getFoodData().foodLevel().get() < 20) {
                    src.offer(Keys.FOOD_LEVEL, 20);
                    src.sendMessage(Text.of(TextColors.DARK_GREEN, "You have been fed!"));
                } else if(src.getFoodData().foodLevel().get() >= 20) {
                    src.sendMessage(Text.of(TextColors.DARK_GREEN, "You are already full!"));
                }
                return CommandResult.success();
            }
            return CommandResult.empty();
        }
        if (source instanceof ConsoleSource) {
            Optional<Player> player = args.getOne("player");
            if(player.isPresent()) {
                if(player.get().getFoodData().foodLevel().get() < 20) {
                    player.get().offer(Keys.FOOD_LEVEL, 20);
                    player.get().sendMessage(Text.of(TextColors.DARK_GREEN, "You have been fed!"));
                    source.sendMessage(Text.of(TextColors.DARK_GREEN, "They have been fed."));
                } else if(player.get().getFoodData().foodLevel().get() >= 20) {
                    source.sendMessage(Text.of(TextColors.DARK_GREEN, "They are already full!"));
                }
                return CommandResult.success();
            }
            source.sendMessage(Text.of(TextColors.DARK_RED, "You can't feed the console!"));
        }
        return CommandResult.empty();
    }
}
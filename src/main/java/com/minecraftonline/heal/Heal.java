package com.minecraftonline.heal;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

@Plugin(id = "heal",
        name = "Heal",
        description = "Heal and feed commands.",
        authors = {
                "44trent3"
        }
)
public class Heal {

    @Inject
    private Logger logger;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        logger.info("Heal has been loaded.");
    }

    @Listener
    public void init(GameInitializationEvent event) {
        registerCommands();
    }

    private void registerCommands() {

        CommandSpec feedCommandSpec = CommandSpec.builder()
                .description(Text.of("Feed command"))
                .permission("heal.command.feed")
                .executor(new FeedCommand())
                .arguments(GenericArguments.optional(GenericArguments.requiringPermission(GenericArguments.player(Text.of("player")), "heal.command.other")))
                .build();

        Sponge.getCommandManager().register(this, feedCommandSpec, "feed");

        CommandSpec healCommandSpec = CommandSpec.builder()
                .description(Text.of("Heal command"))
                .permission("heal.command.heal")
                .executor(new HealCommand())
                .arguments(GenericArguments.optional(GenericArguments.requiringPermission(GenericArguments.player(Text.of("player")), "heal.command.other")))
                .build();

        Sponge.getCommandManager().register(this, healCommandSpec, "heal");
    }

}
